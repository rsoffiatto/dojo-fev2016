package testing

import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by rsoffiatto on 31/01/16.
 */
class CasaTest extends Specification {

    def "porta da casa deve abrir no estilo TDD"() {
        setup:
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [new Janela(), new Janela()])
        casa.abrirPorta(0)

        expect:
        casa.isPortaAberta(0) == true
    }

    def "porta da casa deve abrir no estilo BDD"() {
        given:
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [new Janela(), new Janela()])

        when:
        casa.abrirPorta(0)

        then:
        casa.isPortaAberta(0) == true
    }

    def "porta da casa deve abrir no estilo BDD Com documentation"() {
        given: "Uma casa com duas janelas e uma porta"
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [new Janela(), new Janela()])

        when:"Uma porta é aberta"
        casa.abrirPorta(0)

        then: "Está porta deve passar a estar aberta"
        casa.isPortaAberta(0) == true
    }


    def "esperando exceções"() {
        given:
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [new Janela(), new Janela()])

        when:
        casa.fecharPorta(0)

        then:
        thrown(IllegalStateException)
    }

    def "stub a partir de um mapa"() {
        given:
        def janela = [abrir: { println "Abriu" }, isAberta: true] as Janela
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [janela])

        when:
        casa.abrirJanela(0)

        then:
        casa.isJanelaAberta(0) == true
    }

    def "stub a partir de uma closure"() {
        given:
        def janela = { println "Chamou um método"; true } as Janela
        def casa = new Casa([new Porta(Chave.SEGREDO_1)], [janela])

        when:
        casa.abrirJanela(0)

        then:
        casa.isJanelaAberta(0) == true
    }

    @Unroll
    def "testes parametrizados: #a + #b = #c"() {
        expect:
        a + b == c

        where:
        a  | b  || c
        1  | 2  || 3
        2  | 4  || 6
        -1 | -1 || -2
    }


    def "outro formato de testes parametrizados. Sem Unroll"() {
        expect:
        a + b == c

        where:
        a << [1, 2, -1]
        b << [2, 4, -1]
        c << [3, 6, -2]
    }
}
