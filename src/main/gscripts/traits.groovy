#!/usr/bin/env groovy
import traits.Pessoa


def pessoa = new Pessoa()

pessoa.respirar()

pessoa.tropecar()
pessoa.mamar()

pessoa.respirar()
pessoa.caminhar()

pessoa.respirar()
