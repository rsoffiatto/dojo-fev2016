#!/usr/bin/env groovy

import nomin.carro.Carro
import nomin.carro.Motor
import nomin.carro.Pneu
import nomin.carro.Transmissao
import org.nomin.core.Nomin
import nomin.carro.Porta

def nomin = new Nomin("car2carro.groovy", "drivetrain2transmissao.groovy", "engine2motor.groovy")

def motor = new Motor(familia: "Formula", tamanho: 3.0)
def pneus = ([new Pneu(tipo: Pneu.SLICK)]*4).toArray()
def transmissao = new Transmissao(tipo: Transmissao.MANUAL, numeroDeMarchas: 6)
def portas = [new Porta(lado: Porta.LADO_DIREITO, posicao: Porta.POSICAO_DIANTEIRA),
              new Porta(lado: Porta.LADO_ESQUERDO, posicao: Porta.POSICAO_DIANTEIRA)]
def carro = new Carro(portas: portas, motor: motor, transmissao: transmissao, pneus: pneus)

def racingCar = nomin.map(carro, nomin.car.RacingCar)
println racingCar