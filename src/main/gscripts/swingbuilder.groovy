#!/usr/bin/env groovy
import groovy.swing.SwingBuilder

import javax.swing.JFrame
import java.awt.BorderLayout

new SwingBuilder().edt {
    frame(title: "Meu querido Swing", size: [300, 100], visible: true, defaultCloseOperation: JFrame.EXIT_ON_CLOSE) {
        borderLayout()
        def lbl1 = label(text: "<html>Eu sou um Label <b>original</b></html>", constraints: BorderLayout.NORTH)
        button(text: "Me clique para mudar essa label feia", constraints: BorderLayout.SOUTH, actionPerformed: {
            lbl1.text = "<html>Eu sou uma Label <b>modificada</b>. E muito mais bonita</html>"
        })
    }
}