#!/usr/bin/env groovy

def binding = [a: A, b: B]

def f = new File('build/resources/main/example.template')
def engine = new groovy.text.GStringTemplateEngine()
def template = engine.createTemplate(f).make(binding)
println template.toString()

class A {
    def atributoA
    def atributoB
    def atributoC
}

class B {
    def atributoB
    def atributoC
    def atributoD
}