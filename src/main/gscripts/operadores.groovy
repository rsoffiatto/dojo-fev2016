#!/usr/bin/env groovy

def nulo = null
def naoNulo = 1..5

def sumNulo = nulo?.inject {a, b -> a + b}
def sumNaoNulo = naoNulo?.inject {a, b -> a + b}

println "Navegação null-safe:\n" +
        "nulo: ${sumNulo}\n" +
        "não nulo: ${sumNaoNulo}"

println "Nave espacial: ${15 <=> 10.5}"

println "Elvis: ${ nulo ?: 'É nulo!' } | ${naoNulo ?: 'É nulo!'}"

def upper = "Para o alto! E Avante!".&toUpperCase
println upper
println upper()

def outro = "Outro gato!"
def temCachorro = outro =~ /cachoro/
def temGato = outro =~ /gato/

println temGato
println "Tem cachorro? ${temCachorro ? 'sim' : 'nao'}"
println "Tem gato? ${temGato ? 'sim' : 'nao'}"

def temGatoExatamente = outro ==~ /gato/
def temCachorroMesmo = outro ==~ /.*cachorro.*/
def temGatoMesmo = outro ==~ /.*gato.*/
println temGatoMesmo
println "É gato? ${temGatoExatamente ? 'sim' : 'nao'}"
println "Tem cachorro? ${temCachorroMesmo ? 'sim' : 'nao'}"
println "Tem gato? ${temGatoMesmo ? 'sim' : 'nao'}"

def tudoUpper = ('a'..'f')*.toUpperCase()
println tudoUpper

