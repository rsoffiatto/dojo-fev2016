#!/usr/bin/env groovy
import ast.BuilderExample
import ast.CanonicalExample
import ast.DelegateExample
import ast.ExternalBuilder
import ast.InheritConstructorsExample
import groovy.transform.Canonical

println "--- Canonical ---"

def canonical1 = new CanonicalExample(id: 1, nome: "cano1")
def canonical2 = new CanonicalExample(id: 1, nome: "cano1")
def canonical3 = new CanonicalExample(id: 2, nome: "cano2")

println canonical1.toString()
println canonical2.toString()
println canonical3.toString()

println canonical1 == canonical2
println canonical2 == canonical3

println canonical1.hashCode()

println "\n--- Inherit Contructors ---"

def incont = new InheritConstructorsExample("nome")
println incont

println "\n--- Builder ---"
println new BuilderExample().id(1).nome("nome1").valor(12.5)
println new ExternalBuilder().id(2).nome("nome2").valor(25).build()

println "\n--- Delegate ---"

def delegate = new DelegateExample()
delegate.add("Teste 1")
println "delegate contains 'Teste 1': ${delegate.contains('Teste 1')}"
println delegate