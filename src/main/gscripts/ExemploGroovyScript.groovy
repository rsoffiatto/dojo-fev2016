#!/usr/bin/env groovy

@Grab('com.h2database:h2:1.4.187')
@GrabConfig(systemClassLoader = true)

import groovy.sql.Sql

import java.sql.SQLException

def String JDBC_DRIVER = "org.h2.Driver"
def String DB_URL = "jdbc:h2:mem:teste1;DB_CLOSE_DELAY=-1"
def String USER = "sa"
def PASS = ""

def lista = []
def mapa = [:]

try {
    new File(args[0]).eachLine {
        switch (it) {
            case ~/\d.*/:
                it.tokenize(':').with { mapa[it[0]] = it[1] }
                break
            default:
                lista << it
        }
    }

    def maior = mapa.inject { atual, entry -> entry.key > atual.key ? entry : atual }
    println "O maior valor é ${maior.key} e corresponde a ${maior.value}"

    def sql = Sql.newInstance(DB_URL, USER, PASS, JDBC_DRIVER)

    sql.execute('''create table TESTE (
                        ID identity not null,
                        LINHA varchar(255)
                   );''')
    lista.each { sql.executeInsert("insert into TESTE (LINHA) values (?)", [it]) }
    def unique = sql.firstRow("select count(distinct linha) from TESTE")[0]

    sql.close()

    println "${unique} strings diferentes salvas"
} catch (ClassNotFoundException) {
    println "O driver do banco não está presente"
    System.exit 1
} catch (SQLException e ) {
    println "Erro acessando banco: ${e.getMessage()}"
    System.exit 2
} catch (IOException e ) {
    println "Erro lendo arquivo: ${e.getMessage()}"
    System.exit 3
}