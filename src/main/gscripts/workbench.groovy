#!/usr/bin/env groovy

def comum = A1.metaClass.properties.findAll { it.name != 'class' && B1.metaClass.properties*.name.contains(it.name) }
def aNaoB = A1.metaClass.properties.findAll { !B1.metaClass.properties*.name.contains(it.name) }
def bNaoA = B1.metaClass.properties.findAll { !A1.metaClass.properties*.name.contains(it.name) }

println comum*.name
println aNaoB*.name
println bNaoA*.name

class A1 {
    def atributoA
    def atributoB
    def atributoC
}

class B1 {
    def atributoB
    def atributoC
    def atributoD
}