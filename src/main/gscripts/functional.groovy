#!/usr/bin/env groovy

println "Currying"
println "  left"
def hello = this.&soma.curry("Hello ")
println hello("World!")
println hello("Nurse")

println "\n  right"
def world = this.&soma.rcurry(" World")
println world("Hello Another")
println world("Goodbye cruel")

println "\n  n"
def virgula = this.&soma3.ncurry(1, ",")
println virgula("Hello", "World")
println virgula(1, 2)

println "\n\nMemoization"
def fibN = 25
count = 0
def fibonacci
fibonacci = {
    count++ //Contando quantas vezes esse método é chamado
    it < 2 ? it : fibonacci(it-1) + fibonacci(it-2)
}
println "Cálculo de Fibonacci          : ${fibN} = ${fibonacci(fibN)}. Para isso, método chamado ${count} vezes"

count = 0
def memoFib
memoFib = {
    count++ //Contando quantas vezes esse método é chamado
    it < 2 ? it : memoFib(it-1) + memoFib(it-2)
}.memoize()
println "Cálculo de Fibonacci Memoizado: ${fibN} = ${memoFib(fibN)}. Para isso, método chamado ${count} vezes"

println "\n Exemplo com print  "
println "Sem memoization ${somaEPrint(2, 3)}"
println "Sem memoization ${somaEPrint(2, 3)}"

println "Memoizando"
count = 0
def memoSomaEPrint = this.&somaEPrint.memoize()
(1..1000).each { count++; memoSomaEPrint(2, 3) }
println count

println "\n\nComposição"
def quadrado = {it ** 2}
def metade = {it / 2}
def quadradoDaMetade = quadrado << metade
println "Quadrado da metade de 6: ${quadradoDaMetade(6)}"

def metadeDoQuadrado = quadrado >> metade
println "Metade do quadrado de 6: ${metadeDoQuadrado(6)}"


def soma(def a, def b) {
    a + b
}

def soma3(def a, def b, def c) {
    a + b + c
}

def somaEPrint(def a, def b) {
    println "Somando $a e $b"
    a + b
}