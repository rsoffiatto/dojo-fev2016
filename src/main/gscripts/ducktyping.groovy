#!/usr/bin/env groovy

def pato = new Pato()
def pessoa = new Pessoa()
def aviao = new Aviao()
def submarino = new Submarino()

voar(pato)
nadar(pato)

voar(pessoa)
nadar(pessoa)

voar(aviao)
nadar(aviao)

voar(submarino)
nadar(submarino)

println "Adicionando poder da natação ao avião!"
Aviao.metaClass.nadar = {
    "Agora eu sou um hidroavião!"
}

nadar(aviao)
nadar(new Aviao())

println "Ainda posso fazer algo..."
aviao.metaClass.nadar = Aviao.metaClass.nadar

nadar(aviao)

def voar(algo) {
    println algo.voar()
}

def nadar(algo) {
    println algo.nadar()
}

class Aviao {
    def voar() { "Sou um avião feliz!" }

    def methodMissing(String name, def args) {
        "Este avião não sabe ${name}"
    }
}

class Submarino {
    def nadar() { "Submergindo!" }

    def methodMissing(String name, def args) {
        "Sou um submarino e não sei ${name}"
    }
}

class Pato {
    def voar() { "Pato voando!" }
    def nadar() { "Pato nadando!" }
}

class Pessoa {
    def voar() { "Espere antes eu colocar minha mochila a jato." }
    def nadar() { "Colocando pé de pato!" }
}

