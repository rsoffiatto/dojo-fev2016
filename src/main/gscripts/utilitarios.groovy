#!/usr/bin/env groovy
import groovy.json.JsonOutput
import groovy.xml.DOMBuilder
import groovy.xml.MarkupBuilder
import groovy.xml.XmlUtil
import groovy.xml.dom.DOMCategory

def notes = new XmlSlurper().parse('build/resources/main/example.xml')

println notes.note[0].@date
println "${notes.note[1].from} -> ${notes.note[1].to}"
println XmlUtil.serialize(notes)

def document = DOMBuilder.parse(new FileReader('build/resources/main/example.xml'))
def domNotes = document.documentElement
use(DOMCategory) {
    println domNotes.note.size()
}

def writer = new StringWriter()
def novoXml =  new MarkupBuilder(writer)

novoXml.agenda() {
    contato {
        nome "Alejo"
        telefone(tipo: "celular") {
            ddd 11
            numero "99999-9999"
        }
        telefone(tipo: "casa") {
            ddd 11
            numero "3333-3333"
        }
    }
    contato {
        nome "Janco Tianno"
        telefone(tipo: "trabalho") {
            ddd 21
            numero "2323-3232"
        }
    }
}

println writer.toString()

def json = JsonOutput.toJson([agenda: [
    contato: [
        [nome: "Alejo",
         telefone: [[
                tipo: "celular",
                ddd: 11,
                numero: "99999-9999"
            ],[
                tipo: "casa",
                ddd: 11,
                numero: "3333-3333"
            ]
         ]
        ],
        [nome: "Janco Tianno",
         telefone: [[
                 tipo: "trabalho",
                 ddd: 21,
                 numero: "2323-3232"
         ]]
        ]
    ]
]])

println json
println JsonOutput.prettyPrint(json)