#!/usr/bin/env groovy
import groovyx.gpars.GParsExecutorsPool
import groovyx.gpars.GParsPool
import nomin.car.TyreSet
import nomin.carro.Carro
import nomin.carro.Motor
import nomin.carro.Pneu
import nomin.carro.Transmissao
import org.nomin.core.Nomin
import nomin.carro.Porta

def nomin = new Nomin("car2carro.groovy", "drivetrain2transmissao.groovy", "engine2motor.groovy")

def motor = new Motor(familia: "Formula", tamanho: 3.0)
def pneus = ([new Pneu(tipo: Pneu.SLICK)]*4).toArray()
def transmissao = new Transmissao(tipo: Transmissao.MANUAL, numeroDeMarchas: 6)
def portas = [new Porta(lado: Porta.LADO_DIREITO, posicao: Porta.POSICAO_DIANTEIRA),
              new Porta(lado: Porta.LADO_ESQUERDO, posicao: Porta.POSICAO_DIANTEIRA)]
def carro = new Carro(portas: portas, motor: motor, transmissao: transmissao, pneus: pneus)

println "GParsPool"
GParsPool.withPool(10) {
    (1..1000000).eachParallel {
        def racingCar = nomin.map(carro, nomin.car.RacingCar)
        assert racingCar.tyres == TyreSet.SLICK
        assert racingCar.drivetrain.clutch.numberOfPlates == 1
        assert racingCar.drivetrain.gearBox.numberOfGears == 6
        assert racingCar.engine.displacement == (3.0 * 61.02)
        assert racingCar.name == "From Carro"
        if (it % 100 == 0) println it
    }
}

println "\nGParsExecutorPool"
GParsExecutorsPool.withPool(10) { executorService ->
    (1..20).each {
        def song = "I like to move ${it}, move ${it}"
        executorService << {
            println "Voz 1: ${song}"
            println "Voz 2: ${song}"
        }
    }
}

