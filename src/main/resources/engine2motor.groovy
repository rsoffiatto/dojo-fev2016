import nomin.car.Engine
import nomin.carro.Motor

mappingFor a: Engine, b:Motor

a.displacement = b.tamanho
convert to_a: {it * 61.02},
        to_b: {it / 61.02}

b.familia = {"Formula"}
