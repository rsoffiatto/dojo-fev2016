import nomin.car.Clutch
import nomin.car.Drivetrain
import nomin.car.GearBox
import nomin.carro.Transmissao

mappingFor a:Drivetrain, b:Transmissao

a.clutch = { new Clutch()}
a.clutch.numberOfPlates = 1

a.gearBox = {new GearBox()}
a.gearBox.numberOfGears = b.numeroDeMarchas

b.tipo = { Transmissao.MANUAL }