import nomin.car.Drivetrain
import nomin.car.Engine
import nomin.car.RacingCar
import nomin.car.TyreSet
import nomin.carro.Carro
import nomin.carro.Motor
import nomin.carro.Pneu
import nomin.carro.Porta
import nomin.carro.Transmissao

mappingFor a: RacingCar, b: Carro

a.name = { "From Carro" }
a.engine = b.motor
convert to_a: { mapper.map(it, Engine) },
        to_b: { mapper.map(it, Motor) }

a.tyres = b.pneus[0]
convert to_a: { pneuToTyre(it) }

b.pneus = {tyreToPneu(a.tyres[0])}

a.drivetrain = b.transmissao
convert to_a: { mapper.map(it, Drivetrain) },
        to_b: { mapper.map(it, Transmissao) }

b.portas = {
    [new Porta(lado: Porta.LADO_DIREITO, posicao: Porta.POSICAO_DIANTEIRA), new Porta(lado: Porta.LADO_ESQUERDO, Porta.POSICAO_DIANTEIRA)]
}

def pneuToTyre(pneu) {
    switch (pneu.tipo) {
        case Pneu.SLICK:
            return TyreSet.SLICK;
        case Pneu.ASFALTO:
            return TyreSet.STANDARD_GROOVED
        case Pneu.TERRA:
        case Pneu.MISTO:
            return TyreSet.RALLY
    }
}

def tyreToPneu(TyreSet tyre) {
    switch (tyre) {
        case TyreSet.RALLY:
            return ([new Pneu(tipo: Pneu.TERRA)]*4).toArray()
        case TyreSet.INTERMEDIATE:
        case TyreSet.RAIN:
        case TyreSet.STANDARD_GROOVED:
            return ([new Pneu(tipo: Pneu.ASFALTO)]*4).toArray()
        case TyreSet.SLICK:
            return ([new Pneu(tipo: Pneu.SLICK)]*4).toArray()
    }
}