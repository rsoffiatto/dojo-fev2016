package traits

/**
 * Created by renato on 04/02/16.
 */
trait Bipede extends Animal {

    boolean emMovimento = false

    def caminhar() {
        if (vivo) {
            emMovimento = true
            println "Estou andando"
        }

    }

    def tropecar() {
        if (vivo) {
            emMovimento = true
            println "tropecei"
            vivo = false
            emMovimento = false
        }
    }
}