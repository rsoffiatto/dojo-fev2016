package traits

/**
 * Created by renato on 04/02/16.
 */
trait Animal {

    boolean vivo = true

    def abstract respirar()

}