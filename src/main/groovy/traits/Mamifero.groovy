package traits

/**
 * Created by renato on 04/02/16.
 */
trait Mamifero extends Animal {

    boolean emMovimento = false

    def mamar() {
        if (this.traits_Animal__vivo) {
            emMovimento = true
            println "Estou mamando"
            emMovimento = false
        }
    }

    def engasgarMamando() {
        if (vivo) {
            emMovimento = true
            printl "Me engasguei"
            vivo = false
            emMovimento = false
        }
    }

}