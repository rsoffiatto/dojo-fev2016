package traits

/**
 * Created by renato on 04/02/16.
 */
class Pessoa implements Bipede, Mamifero {

    Pessoa() {
        this.traits_Animal__vivo = true
    }

    @Override
    def respirar() {
        if (!emMovimento) {
            println "Estou respirando"
            if (!vivo) vivo = true
        }
    }
}
