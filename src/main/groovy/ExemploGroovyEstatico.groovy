import groovy.sql.Sql
import groovy.transform.CompileStatic

import java.sql.SQLException

@CompileStatic
class ExemploGroovyEstatico {
    private static final String JDBC_DRIVER = "org.h2.Driver"
    private static final String DB_URL = "jdbc:h2:mem:teste1;DB_CLOSE_DELAY=-1"
    private static final String USER = "sa"
    private static final String PASS = ""

    static void main(String ...args) {
        try {
            def lista = [] as List<String>
            Map<Integer, BigDecimal> mapa = [:]

            new File(args[0]).eachLine {
                switch (it) {
                    case ~/\d.*/:
                        def tokens = it.tokenize(':')
                        mapa[tokens[0] as Integer] = tokens[1] as BigDecimal
                        //Ou
                        //it.tokenize(':').with { mapa[it[0] = it[1]] }
                        break
                    default:
                        lista << it
                }
            }

            def maior = mapa.keySet().inject { def acumulado, def proximo -> acumulado < proximo ? proximo : acumulado }
            println "O maior valor é ${maior} e corresponde a ${mapa[maior]}"

            def sql = Sql.newInstance(DB_URL, USER, PASS, JDBC_DRIVER)

            sql.execute('''create table TESTE (
                                ID identity not null,
                                LINHA varchar(255)
                           );''')
            lista.each { sql.executeInsert("insert into TESTE (LINHA) values (?)", [it] as List) }
            def unique = sql.firstRow("select count(distinct linha) from TESTE")[0]

            sql.close()

            println "${unique} strings diferentes salvas"
        } catch (ClassNotFoundException e) {
            println "O driver do banco não está presente: ${e.getMessage()}"
            System.exit 1
        } catch (SQLException e) {
            println "Erro acessando banco: ${e.getMessage()}"
            System.exit 2
        } catch (IOException e) {
            println "Erro lendo arquivo: ${e.getMessage()}"
            System.exit 3
        }
    }


}
