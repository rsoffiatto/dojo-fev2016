package ast

import groovy.transform.ToString
import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import groovy.transform.builder.SimpleStrategy;

/**
 * Created by renato on 03/02/16.
 */
@Builder(builderStrategy = SimpleStrategy, prefix = '')
@ToString
class BuilderExample {

    Integer id
    String nome
    BigDecimal valor

}

@Builder(builderStrategy = ExternalStrategy, forClass = BuilderExample)
class ExternalBuilder { }
