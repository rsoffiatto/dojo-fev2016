package ast

import groovy.transform.Canonical

/**
 * Created by renato on 03/02/16.
 */
@Canonical
class CanonicalExample {
    Integer id
    String nome
}
