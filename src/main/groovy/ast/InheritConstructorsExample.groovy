package ast

import groovy.transform.InheritConstructors
import groovy.transform.ToString

/**
 * Created by renato on 03/02/16.
 */
@InheritConstructors
@ToString(includeSuperProperties = true )
class InheritConstructorsExample extends Pai {

}

class Pai {

    String nome

    public Pai(nome) {
        this.nome = nome
    }

}

class Filho extends Pai {

    Filho(Object nome) {
        super(nome)
    }
}
