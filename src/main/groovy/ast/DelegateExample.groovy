package ast

import groovy.transform.ToString

/**
 * Created by renato on 03/02/16.
 */
@ToString(includeNames = true)
class DelegateExample {
    @Delegate(interfaces = false, includes = ['size', 'contains', 'add', 'remove'])
    Set set = new HashSet()
}
