import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

public class ExemploJava8 {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:mem:teste1;DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASS = "";

    public static void main(String[] args) {

        try {
            Class.forName(JDBC_DRIVER);


            List<String> lista = new ArrayList<>();
            Map<Integer, BigDecimal> mapa = new HashMap<>();

            Files.lines(Paths.get(args[0])).forEach(linha -> {
                if (Character.isDigit(linha.charAt(0))) {
                    String[] valores = linha.split(":");
                    mapa.put(Integer.valueOf(valores[0]), new BigDecimal(valores[1]));
                } else {
                    lista.add(linha);
                }
            });

            Map.Entry<Integer, BigDecimal> maior = mapa.entrySet().stream()
                    .reduce((a, b) -> b.getKey() > a.getKey() ? b : a).orElse(new AbstractMap.SimpleEntry<>(0, null));
            System.out.println("O maior valor é " + maior.getKey() + " e corresponde a " + maior.getValue());

            try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS)) {
                criarTabela(conn);
                for (String linha : lista) {
                    salvar(conn, linha);
                }
                System.out.println(countUnique(conn) + " strings diferentes salvas");
            }

        } catch (ClassNotFoundException e) {
            System.out.println("O driver do banco não está presente");
            System.exit(1);
        } catch (SQLException e) {
            System.out.println("Erro acessando banco:" + e.getMessage());
            System.exit(2);
        } catch (IOException e) {
            System.out.println("Erro lendo arquivo:" + e.getMessage());
            System.exit(3);
        }

    }

    private static void criarTabela(Connection conn) throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            stmt.execute("create table TESTE (ID identity not null, LINHA varchar(255));");
        }
    }

    private static void salvar(Connection conn, String linha) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("insert into TESTE (LINHA) values (?)")) {
            stmt.setString(1, linha);
            stmt.execute();
        }
    }

    private static int countUnique(Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("select count(distinct linha) from TESTE")) {
            ResultSet rs = stmt.executeQuery();
            rs.next();
            return rs.getInt(1);
        }
    }


}
