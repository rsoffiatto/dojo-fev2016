package testing;

/**
 * Created by rsoffiatto on 31/01/16.
 */
public class Janela {
    private boolean aberta = false;

    public void abrir() {
        if (aberta) throw new IllegalStateException("Não pode abrir uma janela aberta");

        aberta = true;
    }

    public void fechar() {
        if (!aberta) throw new IllegalStateException("Não pode fechar uma janela fechada");

        aberta = false;
    }

    public boolean isAberta() {
        return aberta;
    }
}
