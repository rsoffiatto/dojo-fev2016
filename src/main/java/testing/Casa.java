package testing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by rsoffiatto on 31/01/16.
 */
public class Casa {

    private List<Porta> portas;
    private List<Janela> janelas;

    public Casa(List<Porta> portas, List<Janela> janelas) {
        this.portas = Collections.unmodifiableList(new ArrayList<>(portas));
        this.janelas = Collections.unmodifiableList(new ArrayList<>(janelas));
    }

    public void fecharJanela(int numero) {
        janelas.get(numero).fechar();
    }

    public void abrirJanela(int numero) {
        janelas.get(numero).abrir();
    }

    public boolean isJanelaAberta(int numero) {
        return janelas.get(numero).isAberta();
    }

    public void fecharPorta(int numero) {
        portas.get(numero).fechar();
    }

    public void abrirPorta(int numero) {
        portas.get(numero).abrir();
    }

    public boolean isPortaAberta(int numero) {
        return portas.get(numero).isAberta();
    }

    public void trancarPorta(int numero, Chave chave) {
        portas.get(numero).trancar(chave);
    }

    public void destrancarPorta(int numero, Chave chave) {
        portas.get(numero).destrancar(chave);
    }

    public boolean isPortaTrancada(int numero) {
        return portas.get(numero).isTrancada();
    }

}
