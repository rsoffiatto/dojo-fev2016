package testing;

/**
 * Created by rsoffiatto on 31/01/16.
 */
public class Porta {
    private boolean aberta = false;
    private boolean trancada = false;
    private Chave chave;

    public Porta(Chave chave) {
        this.chave = chave;
    }

    public void abrir() {
        if (aberta) throw new IllegalStateException("Não pode abrir uma janela aberta");
        if (trancada) throw new IllegalStateException("Não pode abrir uma janela trancada");

        aberta = true;
    }

    public void fechar() {
        if (!aberta) throw new IllegalStateException("Não pode fechar uma janela fechada");

        aberta = false;
    }

    public boolean isAberta() {
        return aberta;
    }

    public void trancar(Chave chave) {
        if (this.chave != chave) throw new IllegalArgumentException("Chave errada");
        if (this.trancada) throw new IllegalStateException("Não pode trancar uma porta trancada");
        if (this.aberta) throw new IllegalStateException("Não pode trancar uma porta aberta");

        this.trancada = true;
    }

    public void destrancar(Chave chave) {
        if (this.chave != chave) throw new IllegalArgumentException("Chave errada");
        if (!this.trancada) throw new IllegalStateException("Não pode destrancar uma porta destrancada");

        this.trancada = false;
    }

    public boolean isTrancada() {
        return trancada;
    }
}
