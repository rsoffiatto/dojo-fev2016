package nomin.carro;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class Carro {

    private List<Porta> portas = new ArrayList<>();
    private Pneu[] pneus = new Pneu[4];
    private Motor motor;
    private Transmissao transmissao;

    public List<Porta> getPortas() {
        return new ArrayList<>(portas);
    }

    public void setPortas(List<Porta> portas) {
        this.portas.addAll(portas);
    }

    public Pneu[] getPneus() {
        return Arrays.copyOf(pneus, 4);
    }

    public void setPneus(Pneu[] pneus) {
        this.pneus = Arrays.copyOf(pneus, 4);
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public Transmissao getTransmissao() {
        return transmissao;
    }

    public void setTransmissao(Transmissao transmissao) {
        this.transmissao = transmissao;
    }
}
