package nomin.carro;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class Motor {

    //Tamanho em litros
    private double tamanho;
    private String familia;

    public double getTamanho() {
        return tamanho;
    }

    public void setTamanho(double tamanho) {
        this.tamanho = tamanho;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }
}
