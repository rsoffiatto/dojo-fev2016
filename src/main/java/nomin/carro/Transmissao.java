package nomin.carro;

/**
 * Created by renato on 03/02/16.
 */
public class Transmissao {

    public static final int MANUAL = 1;
    public static final int AUTOMATICA = 2;

    private int numeroDeMarchas;
    private int tipo;

    public Transmissao() {}

    public int getNumeroDeMarchas() {
	return numeroDeMarchas;
    }

    public void setNumeroDeMarchas(int numeroDeMarchas) {
	this.numeroDeMarchas = numeroDeMarchas;
    }

    public int getTipo() {
	return tipo;
    }

    public void setTipo(int tipo) {
	this.tipo = tipo;
    }
}
