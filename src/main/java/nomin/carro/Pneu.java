package nomin.carro;

/**
 * Created by renato on 03/02/16.
 */
public class Pneu {

    public static final int SLICK = 1;
    public static final int ASFALTO = 2;
    public static final int MISTO = 3;
    public static final int TERRA = 4;

    private int tipo;

    public Pneu() {
    }

    public int getTipo() {
	return tipo;
    }

    public void setTipo(int tipo) {
	this.tipo = tipo;
    }
}
