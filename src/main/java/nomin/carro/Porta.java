package nomin.carro;

/**
 * Created by renato on 03/02/16.
 */
public class Porta {

    public static final String LADO_DIREITO      = "D";
    public static final String LADO_ESQUERDO     = "E";
    public static final String CENTRO            = "C";
    public static final String POSICAO_DIANTEIRA = "F";
    public static final String POSICAO_TRASEIRA  = "T";

    private String lado;
    private String posicao;

    public Porta() {
    }

    public String getPosicao() {
	return posicao;
    }

    public void setPosicao(String posicao) {
	this.posicao = posicao;
    }

    public String getLado() {
	return lado;
    }

    public void setLado(String lado) {
	this.lado = lado;
    }
}
