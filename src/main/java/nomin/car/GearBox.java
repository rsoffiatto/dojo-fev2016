package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class GearBox {

    private int numberOfGears;

    public int getNumberOfGears() {
	return numberOfGears;
    }

    public void setNumberOfGears(int numberOfGears) {
	this.numberOfGears = numberOfGears;
    }

    @Override
    public String toString() {
        return "GearBox{" +
                "numberOfGears=" + numberOfGears +
                '}';
    }
}
