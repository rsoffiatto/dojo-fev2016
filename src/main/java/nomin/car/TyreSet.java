package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public enum TyreSet {
    SLICK,
    RAIN,
    INTERMEDIATE,
    STANDARD_GROOVED,
    RALLY
}
