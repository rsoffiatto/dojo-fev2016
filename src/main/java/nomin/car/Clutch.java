package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class Clutch {

    private int numberOfPlates;

    public int getNumberOfPlates() {
	return numberOfPlates;
    }

    public void setNumberOfPlates(int numberOfPlates) {
	this.numberOfPlates = numberOfPlates;
    }

    @Override
    public String toString() {
        return "Clutch{" +
                "numberOfPlates=" + numberOfPlates +
                '}';
    }
}
