package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class RacingCar {

    private String name;
    private Engine engine;
    private TyreSet tyres;
    private Drivetrain drivetrain;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Engine getEngine() {
	return engine;
    }

    public void setEngine(Engine engine) {
	this.engine = engine;
    }

    public TyreSet getTyres() {
	return tyres;
    }

    public void setTyres(TyreSet tyres) {
	this.tyres = tyres;
    }

    public Drivetrain getDrivetrain() {
	return drivetrain;
    }

    public void setDrivetrain(Drivetrain drivetrain) {
	this.drivetrain = drivetrain;
    }

    @Override
    public String toString() {
        return "RacingCar{" +
                "name='" + name + '\'' +
                ", engine=" + engine +
                ", tyres=" + tyres +
                ", drivetrain=" + drivetrain +
                '}';
    }
}
