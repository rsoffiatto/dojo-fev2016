package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class Drivetrain {
    private GearBox gearBox;
    private Clutch clutch;

    public GearBox getGearBox() {
	return gearBox;
    }

    public void setGearBox(GearBox gearBox) {
	this.gearBox = gearBox;
    }

    public Clutch getClutch() {
	return clutch;
    }

    public void setClutch(Clutch clutch) {
	this.clutch = clutch;
    }

    @Override
    public String toString() {
        return "Drivetrain{" +
                "gearBox=" + gearBox +
                ", clutch=" + clutch +
                '}';
    }
}
