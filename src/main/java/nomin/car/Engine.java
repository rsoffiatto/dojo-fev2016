package nomin.car;

/**
 * Created by rsoffiatto on 01/02/16.
 */
public class Engine {

    //Displacement in cu
    private double displacement;

    public double getDisplacement() {
	return displacement;
    }

    public void setDisplacement(double displacement) {
	this.displacement = displacement;
    }

    @Override
    public String toString() {
        return "Engine{" +
                "displacement=" + displacement +
                '}';
    }
}
